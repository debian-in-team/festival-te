;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                                             ;;
;;;  Lexical Analysis: lexical lookup, letter-to-sound rules (words to phones)  ;;
;;;                                                                             ;;
;;;  Copyright (c) 2005, Chaitanya Kamisetty <chaitanya@atc.tcs.co.in>          ;;
;;;                                                                             ;;
;;;  This program is a part of festival-te.					;;
;;;  										;;
;;;  festival-te is free software; you can redistribute it and/or modify        ;;
;;;  it under the terms of the GNU General Public License as published by	;;
;;;  the Free Software Foundation; either version 2 of the License, or		;;
;;;  (at your option) any later version.					;;
;;;										;;
;;;  This program is distributed in the hope that it will be useful,		;;
;;;  but WITHOUT ANY WARRANTY; without even the implied warranty of		;;
;;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the		;;
;;;  GNU General Public License for more details.				;;
;;;										;;
;;;  You should have received a copy of the GNU General Public License		;;
;;;  along with this program; if not, write to the Free Software		;;
;;;  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA  ;;
;;;										;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (telugu_addenda)
  "(telugu_addenda)
Basic lexicon should (must ?) basic letters and punctuation."
;; Basic punctuation must be in with nil pronunciation
(lex.add.entry '("'" punc nil))
(lex.add.entry '(":" punc nil))
(lex.add.entry '(";" punc nil))
(lex.add.entry '("," punc nil))
(lex.add.entry '("." punc nil))
(lex.add.entry '("-" punc nil))
(lex.add.entry '("\"" punc nil))
(lex.add.entry '("`" punc nil))
(lex.add.entry '("?" punc nil))
(lex.add.entry '("!" punc nil))
(lex.add.entry '("(" punc nil))
(lex.add.entry '(")" punc nil))
(lex.add.entry '("{" punc nil))
(lex.add.entry '("}" punc nil))
(lex.add.entry '("[" punc nil))
(lex.add.entry '("]" punc nil))
)

(lts.ruleset
 telugu
  ( 
	; Matras can be formed by (OCT340 OCT260 MAT1 )  and (OCT340 OCT260 MAT2)
	( OCT340 � ) 
	( OCT260 � )
	( OCT261  � )
	( MAT1   �  � )
	( MAT2  � � � � � � � �   � � � � � )
  )
  (
;; single vowels
( [ � � � ] = a ) ;telugu letter A
( [ � � � ] = aa ) ;telugu letter AA
( [ � � � ] = ih ) ;telugu letter I
( [ � � � ] = iy ) ;telugu letter II
( [ � � � ] = uh ) ;telugu letter U
( [ � � � ] = uw ) ;telugu letter UU 
( [ � � � ] = r uh ) ;telugu letter vocalic R 
( [ � � � ]  = r uw ) ;telugu letter vocalic RR 
([ � � � ] =  l uh ) ; telugu letter vocalic L (using 'lu' sound)
([ � � � ] = l uw ) ; telugu letter vocalic LL (uinsg 'luu' sound)
( [ � � � ] = eh ) ;telugu letter E
( [ � � � ] = ee ) ;telugu letter EE
( [ � � � ] = ay ) ;telugu letter AI
( [ � � � ] = oh )  ;telugu letter O 
( [ � � � ] = oo )  ;telugu letter OO 
( [ � � � ] = aw ) ;telugu letter AU
( [ � � � ]  = m ) ;telugu sign anusvara (sunna) (using 'ma' sound)
([ � � � ] = h a ) ;telugu sign visarga (using 'ha' sound)
;; consonants in half forms i.e. consonant + halant
( [ � � � � � � ]  = k ) ;telugu letter K
( [ � � � � � � ] = kh ) ;telugu letter KH
( [ � � � � � � ] = g ) ;telugu letter G
( [ � � � � � � ] = g h ) ;telugu letter GH
( [ � � � � � � ] = n y ) ;telugu letter NG
( [ � � � � � � ] = ch ) ;telugu letter C
( [ � � � � � � ] = ch h )	;telug letter CH (missing in phoneset) 
( [ � � � � � � ] = j ) ;telugu letter J
( [ � � � � � � ] = j h )	;telugu letter JH (missing in phoneset)
( [ � � � � � � ] = eh n eh )	;telugu letter NY (missing in phoneset)
( [ � � � � � � ] = T ) ;telugu letter TT
( [ � � � � � � ] = T h )	;telugu letter TTH (missing in phoneset)
( [ � � � � � � ] = D ) ;telugu letter DD
( [ � � � � � � ] = D h ) ;telugu letter DDH (missing in phoneset)
( [ � � � � � � ] = N ) ;telugu letter NN (missing in phoneset)
( [ � � � � � � ] = th )	;telugu letter T (missing in phoneset)
( [ � � � � � � ] = tth ) ;telugu letter TH
( [ � � � � � � ] = dh )	;telugu letter D (missing in phoneset)
( [ � � � � � � ] = ddh )	;telugu letter DH
( [ � � � � � � ] = n ) ;telugu letter N
( [ � � � � � � ] = p ) ;telugu letter P
( [ � � � � � � ] = f )	;telugu letter PH
( [ � � � � � � ] = b ) ;telugu letter B
( [ � � � � � � ] = bh ) ;telugu letter BH
( [ � � � � � � ] = m ) ;telugu letter M
( [ � � � � � � ] = y ) ;telugu letter Y
( [ � � � � � � ] = r ) ;telugu letter R
([ � � � � � � ] =  r ) ;telugu letter RR (missing in phoneset)
( [ � � � � � � ] = l ) ;telugu letter L 
( [ � � � � � � ] = L ) ;telugu letter LL
( [ � � � � � � ] = v ) ;telugu letter V
( [ � � � � � � ] = sh ) ;telugu letter SH
( [ � � � � � � ] = sh h ) ;telugu letter SS
( [ � � � � � � ] = s ) ;telugu letter S (missing in phoneset)
( [ � � � � � � ] = h ) ;telugu letter H
;;consonants occuring as vattulu
;; matches the regex [consonant]{consonant}
( [ � � � ]  OCT340 OCT260 MAT1  =  k ) ;telugu letter K
( [ � � � ]  OCT340 OCT260 MAT1 = kh ) ;telugu letter KH
( [ � � � ]   OCT340 OCT260 MAT1 = g ) ;telugu letter G
( [ � � � ]   OCT340 OCT260 MAT1 = g h ) ;telugu letter GH
( [ � � � ]  OCT340 OCT260 MAT1 = n y ) ;telugu letter NG
( [ � � � ]   OCT340 OCT260 MAT1 = ch ) ;telugu letter C
( [ � � � ]  OCT340 OCT260 MAT1  = ch h ) ;telugu letter CH
( [ � � � ]  OCT340 OCT260 MAT1  = j ) ;telugu letter J
( [ � � � ]   OCT340 OCT260 MAT1 = j h ) ;telugu letter JH
( [ � � � ]   OCT340 OCT260 MAT1 = eh n eh ) ;telugu letter NY
( [ � � � ]   OCT340 OCT260 MAT1 = T ) ;telugu letter TT
( [ � � � ]   OCT340 OCT260 MAT1 = T h ) ;telugu letter TTH
( [ � � � ]   OCT340 OCT260 MAT1 = D ) ;telugu letter DD
( [ � � � ]   OCT340 OCT260 MAT1 = D h ) ;telugu letter DDH
( [ � � � ]   OCT340 OCT260 MAT1 = N ) ;telugu letter NN
( [ � � � ]   OCT340 OCT260 MAT1 = th ) ;telugu letter T
( [ � � � ]  OCT340 OCT260 MAT1  = tth ) ;telugu letter TH
( [ � � � ]   OCT340 OCT260 MAT1 = dh ) ;telugu letter D
( [ � � � ]   OCT340 OCT260 MAT1 = ddh ) ;telugu letter DH
( [ � � � ]   OCT340 OCT260 MAT1 = n ) ;telugu letter N
( [ � � � ]   OCT340 OCT260 MAT1 = p ) ;telugu letter P
( [ � � � ]   OCT340 OCT260 MAT1 = f ) ;telugu letter PH
( [ � � � ]   OCT340 OCT260 MAT1 = b ) ;telugu letter B
( [ � � � ]   OCT340 OCT260 MAT1 = bh ) ;telugu letter BH
( [ � � � ]   OCT340 OCT260 MAT1 = m ) ;telugu letter M
( [ � � � ]   OCT340 OCT260 MAT1 = y ) ;telugu letter Y
( [ � � � ]   OCT340 OCT260 MAT1 = r ) ;telugu letter R
( [ � � � ] OCT340 OCT260 MAT1 = r ) ;telugu letter RR
( [ � � � ]  OCT340 OCT260 MAT1  = l) ;telugu letter L
( [ � � � ]   OCT340 OCT260 MAT1 = L ) ;telugu letter LL
( [ � � � ]   OCT340 OCT260 MAT1 = v ) ;telugu letter V
( [ � � � ]   OCT340 OCT260 MAT1 = sh ) ;telugu letter SH
( [ � � � ]   OCT340 OCT260 MAT1 = sh h ) ;telugu letter SS
( [ � � � ]   OCT340 OCT260 MAT1 = s ) ;telugu letter S
( [ � � � ]   OCT340 OCT260 MAT1 = h ) ;telugu letter H
( [ � � � ]   OCT340 OCT261 MAT2    =  k ) ;telugu letter K
( [ � � � ]  OCT340 OCT261 MAT2   = kh ) ;telugu letter KH
( [ � � � ]  OCT340 OCT261 MAT2   = g ) ;telugu letter G
( [ � � � ]   OCT340 OCT261 MAT2  = g h ) ;telugu letter GH
( [ � � � ]   OCT340 OCT261 MAT2  = n y ) ;telugu letter NG
( [ � � � ]   OCT340 OCT261 MAT2  = ch ) ;telugu letter C
( [ � � � ]  OCT340 OCT261 MAT2  = ch h ) ;telugu letter CH
( [ � � � ]  OCT340 OCT261 MAT2  = j ) ;telugu letter J
( [ � � � ]   OCT340 OCT261 MAT2  = j h ) ;telugu letter JH
( [ � � � ]   OCT340 OCT261 MAT2  = eh n eh ) ;telugu letter NY
( [ � � � ]  OCT340 OCT261 MAT2   = T ) ;telugu letter TT
( [ � � � ]   OCT340 OCT261 MAT2  = T h ) ;telugu letter TTH
( [ � � � ]   OCT340 OCT261 MAT2  = D ) ;telugu letter DD
( [ � � � ]   OCT340 OCT261 MAT2  = D h ) ;telugu letter DDH
( [ � � � ]  OCT340 OCT261 MAT2   = N ) ;telugu letter NN
( [ � � � ]   OCT340 OCT261 MAT2  = th ) ;telugu letter T
( [ � � � ]  OCT340 OCT261 MAT2   = tth ) ;telugu letter TH
( [ � � � ]   OCT340 OCT261 MAT2  = dh ) ;telugu letter D
( [ � � � ]   OCT340 OCT261 MAT2  = ddh ) ;telugu letter DH
( [ � � � ]   OCT340 OCT261 MAT2  = n ) ;telugu letter N
( [ � � � ]   OCT340 OCT261 MAT2  = p ) ;telugu letter P
( [ � � � ]   OCT340 OCT261 MAT2  = f ) ;telugu letter PH
( [ � � � ]   OCT340 OCT261 MAT2  = b ) ;telugu letter B
( [ � � � ]   OCT340 OCT261 MAT2  = bh ) ;telugu letter BH
( [ � � � ]   OCT340 OCT261 MAT2  = m ) ;telugu letter M
( [ � � � ]   OCT340 OCT261 MAT2  = y ) ;telugu letter Y
( [ � � � ]  OCT340 OCT261 MAT2   = r ) ;telugu letter R
([ � � � ] OCT340 OCT261 MAT2  = r ) ;telugu letter RR
( [ � � � ]   OCT340 OCT261 MAT2  = l) ;telugu letter L
( [ � � � ]  OCT340 OCT261 MAT2   = L ) ;telugu letter LL
( [ � � � ]   OCT340 OCT261 MAT2  = v ) ;telugu letter V
( [ � � � ]   OCT340 OCT261 MAT2  = sh ) ;telugu letter SH
( [ � � � ]   OCT340 OCT261 MAT2  = sh h ) ;telugu letter SS
( [ � � � ]  OCT340 OCT261 MAT2   = s ) ;telugu letter S
( [ � � � ]   OCT340 OCT261 MAT2  = h ) ;telugu letter H
;consonants
( [ � � � ]   =  k a ) ;telugu letter KA
( [ � � � ] = kh a ) ;telugu letter KHA
( [ � � � ] = g a ) ;telugu letter GA
( [ � � � ] = g h a ) ;telugu letter GHA
( [ � � � ] = n y a ) ;telugu letter NGA
( [ � � � ] = ch a ) ;telugu letter CA
( [ � � � ] = ch h a ) ;telugu letter CHA
( [ � � � ] = j a ) ;telugu letter JA
( [ � � � ] = j h a ) ;telugu letter JHA
( [ � � � ] = eh n eh a ) ;telugu letter NYA
( [ � � � ] = T a ) ;telugu letter TTA
( [ � � � ] = T h a ) ;telugu letter TTHA
( [ � � � ] = D a ) ;telugu letter DDA
( [ � � � ] = D h a ) ;telugu letter DDHA
( [ � � � ] = N a ) ;telugu letter NNA
( [ � � � ] = th a ) ;telugu letter TA
( [ � � � ] = tth a ) ;telugu letter THA
( [ � � � ] = dh a ) ;telugu letter D
( [ � � � ] = ddh a ) ;telugu letter DHA
( [ � � � ] = n a ) ;telugu letter NA
( [ � � � ] = p a ) ;telugu letter PA
( [ � � � ] = f a ) ;telugu letter PHA
( [ � � � ] = b a ) ;telugu letter BA
( [ � � � ] = bh a ) ;telugu letter BHA
( [ � � � ] = m a ) ;telugu letter MA
( [ � � � ] = y a ) ;telugu letter YA
( [ � � � ] = r a ) ;telugu letter RA
([ � � � ] = r a ) ;telugu letter RRA
( [ � � � ] = l a ) ;telugu letter LA
( [ � � � ] = L a ) ;telugu letter LLA
( [ � � � ] = v a ) ;telugu letter VA
( [ � � � ] = sh a ) ;telugu letter SHA
( [ � � � ] = sh h a ) ;telugu letter SSA
( [ � � � ] = s a ) ;telugu letter SA
( [ � � � ] = h a ) ;telugu letter HA
;;dependent vowels
([ � � � � � � ] = ay ) ;telugu vowel sign AI 0C46 (E)+ 0C56(AI Length Mark)
( [ � � � ] =  aa )  ;telugu vowel sign AA
( [ � � � ] =  ih ) ;telugu vowel sign I
( [ � � � ] = iy ) ;telugu vowel sign II
( [ � � � ] = uh ) ;telugu vowel sign U
( [ � � � ] = uw ) ;telugu vowel sign UU
( [ � � � ] = r eh )  ;telugu vowel sign vocalic R
( [ � � � ] = r ee )  ;telugu vowel sign vocalic RR
( [ � � � ] = eh ) ;telugu vowel sign E
( [ � � � ] = ee ) ;telugu vowel sign EE
( [ � � � ] = ay ) ;telugu vowel sign AI
( [ � � � ] = oh ) ;telugu vowel sign O
( [ � � � ] = oo ) ;telugu vowel sign OO
( [ � � � ] = aw ) ;telugu vowel sign AU
( [ � � � ] = )  ;ignoring halants
( [ � � � ] = ) ;ignoring chandrabindu
( [ � � � ] = ) ;ignoring telugu length mark U+0C55
;;ignoring reserved characters
;( [  � � � ] = ) ;characterU+0C00
;( [  � � � ] = ) ;characterU+0C04
;( [  � � � ] = ) ;characterU+0C0D
;( [  � � � ] = ) ;characterU+0C11
;( [  � � � ] = ) ;characterU+0C29
;( [   ] = ) ;characterU+0C34
;( [  � � � ] = ) ;characterU+0C3A
;( [  � � � ] = ) ;characterU+0C3B
;( [  ] = ) ;characterU+0C3C
;( [  � � ] = ) ;characterU+0C3D
;( [  � � ] = ) ;characterU+0C
   )
)

;;; Lexicon definition
(lex.create "telugu")
(lex.set.phoneset "telugu")

(define (telugu_lts_function word features)
  "(telugu_lts_function WORD FEATURES)
 Using letter to sound rules to bulid a telugu pronunciation of WORD."
   (list word
	 nil
          (lex.syllabify.phstress (lts.apply (downcase word) 'telugu))))
(lex.set.lts.method 'telugu_lts_function)
(telugu_addenda)

(provide 'telugu_lex)


