festival-te (0.3.3-6) unstable; urgency=low

  [ Kartik Mistry ]
  * Team upload.
  * Updated Standards-Version to 4.6.1
  * Updated debian/copyright.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + festvox-te-nsk: Add Multi-Arch: foreign.
  * Set upstream metadata fields: Archive, Repository.
  * Update standards version to 4.6.0, no changes needed.
  * Remove constraints unnecessary since buster (oldstable):
    + festival-te: Drop versioned constraint on festival in Depends.

 -- Kartik Mistry <kartik@debian.org>  Thu, 24 Nov 2022 17:51:12 +0530

festival-te (0.3.3-5) unstable; urgency=low

  * Team upload.
  * Updated debian/watch.
  * debian/control:
    + Updated Maintainer email address.
    + Removed inactive uploaders.
    + Updated Vcs-* URLs.
    + Switched to debhelper-compat.
    + Updated Standards-Version to 4.5.0
    + Added 'Rules-Requires-Root' field.
    + Removed Pre-Depends fields.
  * debian/rules:
    + Removed custom compression.
  * Updated debian/copyright.
  * Added debian/gitlab-ci.yml.

 -- Kartik Mistry <kartik@debian.org>  Wed, 06 May 2020 13:23:46 +0530

festival-te (0.3.3-4) unstable; urgency=low

  * Team upload.
  * debian/control:
    + Added Pre-Depends on dpkg (>= 1.15.6~)
    + Minimum version of debhelper required is changed to 9
  * debian/rules:
    + Introduced dh for simple rules file
    + Introduced xz compression
  * Set debian/compat to 9, previously this was 5
  * debian/copyright:
    + Removed extra Copyright: prefix from debian folder copyright
      block.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Sat, 23 Jun 2012 22:28:03 +0530

festival-te (0.3.3-3) unstable; urgency=low

  * Team upload.
  * debian/control:
    + Bumped debhelper version to 8.0.0
    + Updated Standards-Version to 3.9.3
    + Update the Vcs fields to Git.
    + Updated Homepage field.
  * debian/copyright:
    + Updated debian/copyright to copyright-format 1.0

 -- AbdulKarim Memon <abdulkarimmemon@gmail.com>  Mon, 05 Mar 2012 11:47:17 +0530

festival-te (0.3.3-2) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Added ${misc:Depends} to binary packages
    + Updated Standards-Version to 3.8.4 (no changes needed)
  * debian/rules:
    + Install upstream changelog file
  * debian/copyright:
    + Don't point to versionless license text

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 27 Feb 2010 22:11:23 +0530

festival-te (0.3.3-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + Updated Standards-Version to 3.8.2
  * debian/copyright:
    + Updated to use correct copyright symbol
  * Added missing debian/watch file

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Wed, 24 Jun 2009 13:12:05 +0530

festival-te (0.3.2-6) unstable; urgency=low

  * debian/control:
    + Fixed typo. Thanks to Y Giridhar Appaji Nag
      <giridhar@appaji.net> for patch (Closes: #469163)
  * debian/copyright:
    + Updated GPL license link in Debian

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Tue, 08 Apr 2008 09:32:22 +0530

festival-te (0.3.2-5) unstable; urgency=low

  * debian/control:
    + Added Homepage entry
    + Updated Standards-Version to 3.7.3
    + Updated package descriptions
    + Added VCS-* fields
  * Added debian/watch file
  * debian/copyright:
    + Moved copyright out of license section
  * debian/changelog:
    + Fixed typo of compatibility from previous upload
    + Fixed empty spaces, tabs and lines indented to 80 characters

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 26 Jan 2008 15:07:05 +0530

festival-te (0.3.2-4) unstable; urgency=low

  [Kartik Mistry]
  * Upload as Debian-IN Team
  * Set maintainer address to Debian-IN
  * Updated debhelper compatibility to 5
  * debian/copyright: updated according to standard copyright file format
  * debian/README.Debian: added and mention that you need to apply given
    patches to make package really work
  * debian/rules: moving to plain debhelper from cdbs
  * debian/festvox-te-nsk.docs: removed example/ entry, we are using rules for
    this now
  * debian/festvox-te-nsk.dirs: added required entries
  * debian/contol: more better descriptions of packages, cleanups
  * debian/changelog: removed couple of empty lines at end

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Tue, 22 May 2007 17:29:01 +0530

festival-te (0.3.2-3) unstable; urgency=low

  * As Sunil Mohan <sunil@atc.tcs.co.in> says festival-te can't run without
    festivox-te-nsk.festival-te is like the program and festivox-te-nsk is like
    the data. Also some package other than festival-te can use festvox-te-nsk.
    So we are changing the recommends of festival-te section to depends on
    festvox-te-nsk and depends of festvox-te-nsk section to recommends on
    festival-te in debian/control. Closes: #384673

  * In debian/control Build-Depends-Indep is changed to Build-Depends according
    to Debian Policy Manual, section 7.6 for CDBS and Debhelper.

 -- Prasad Ramamurthy Kadambi <prasad.kadambi@gmail.com>  Fri,  8 Sep 2006 22:46:05 +0530

festival-te (0.3.2-2) unstable; urgency=low

  * There is a circular dependency between festival-te and festvox-te-nsk.
    Circular dependencies are known to cause problems during upgrade, so we are
    avoiding it by making suitable changes in debian/control file by changing
    the dependency on festvox-te-nsk to a recommends in festival-te's section.
    Closes: #384673

  * festvox-te-nsk Provides: festival-voice

 -- Prasad Ramamurthy Kadambi <prasad.kadambi@gmail.com>  Thu, 31 Aug 2006   23:38:05 +0530

festival-te (0.3.2-1) unstable; urgency=low

  * Initial release Closes: #357307,#357310

  * Enrico Zini <enrico@debian.org > has patched telugu_NSK_diphone.scm because
    gnome-speech will ask the voices for the 'coding' attribute, defaulting to
    latin1 if they don't provide it.  Latin1 sucks, but most voices read latin1
    at the moment.Once all festival voices support the coding attribute, the
    default could be moved back to UTF-8.This could break future gnome-speech
    interaction with the Telugu voice.

 -- Prasad Ramamurthy Kadambi <prasad.kadambi@gmail.com>  Mon, 14 Aug 2006  21:48:11 +0530
